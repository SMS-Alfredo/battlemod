# SRB2 Battle - Created by CobaltBW

## Game Design and lead scripting:
- CobaltBW

## v9 scripting:
- Krabs

## Scripting contributions:
- Anonymous
- Denny
- Kays
- Lat'
- Lianvee
- LJ Sonic
- reverbal
- SMS Alfredo
- Wane

## Sprite artists:
- DirkTheHusky: Instashield
- FlyingNosaj: Tails Doll
- Inazuma: Stun Break
- MotorRoach: Tails special move
- Panthonas: Original Tails Doll

## Resources used:
- Kays - Object respawning scripts
- Flame - 2D camera script
- TehRealSalt - Original Fang popgun aim/fire re-emulation script

## Map design:
- CobaltBW - Layouts
- Krabs - Layouts and visual design
- FlareBlade93 - Pumpkin Peak Zone

## SFX:
- Krabs

## Beta testing:
- Alice
- AxelMoon
- Chrispy
- Chase
- FlareBlade93
- Inazuma
- Kays
- Krabs
- Nev3r
- sphere

## Resources ripped from games:
- Chess pieces pulled from Secret of Mana, "Poltergeist" (spritesheet from The Spriters Resource)
- Metal Sonic "claw" sprites pulled from RPG Maker 2003
- Battle "shield" pulled from Sonic Battle
- Various sound effects pulled from Super Smash Bros. Melee and Super Smash Bros. Ultimate (from The Sounds Resource)
